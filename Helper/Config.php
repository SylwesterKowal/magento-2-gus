<?php

namespace Kowal\Gus\Helper;

class Config extends \Magento\Framework\App\Helper\AbstractHelper
{

    const SECTIONS = 'nipfromgus';   // module name
    const GROUPS = 'settings';        // setup general
    const GROUPS_RETURN = 'return';        // setup general

    /**
     * @var AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var Random
     */
    protected $mathRandom;

    public function __construct(
        \Magento\Framework\App\Helper\Context      $context,
        \Magento\Framework\ObjectManagerInterface  $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\AuthorizationInterface  $authorization,
        \Magento\Framework\Math\Random             $random
    )
    {
        $this->_authorization = $authorization;
        $this->mathRandom = $random;
        $this->storeManager = $storeManager;

        parent::__construct($context);
    }

    public function getConfig($cfg = null, $store_id = 0)
    {
        return $this->scopeConfig->getValue(
            $cfg,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );
    }

    public function getGeneralCfg($cfg = null, $store_id = 0)
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS . '/' . self::GROUPS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );

        if (isset($config[$cfg])) return $config[$cfg];
        return $config;
    }

    public function getReturnCfg($cfg = null, $store_id = 0)
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS . '/' . self::GROUPS_RETURN,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );

        if (isset($config[$cfg])) return $config[$cfg];
        return $config;
    }


    /**
     * @return string
     * @throws LocalizedException
     */
    public function getLoginToken()
    {
        return $this->mathRandom->getUniqueHash();
    }

    /**
     * @param Customer $customer
     *
     * @return StoreInterface|null
     * @throws NoSuchEntityException
     */
    public function getStore($customer)
    {
        if ($storeId = $customer->getStoreId()) {
            return $this->storeManager->getStore($storeId);
        }

        return $this->storeManager->getDefaultStoreView();
    }

}
