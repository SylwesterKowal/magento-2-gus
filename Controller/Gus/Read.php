<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Gus\Controller\Gus;

use GusApi\Exception\InvalidUserKeyException;
use GusApi\Exception\NotFoundException;
use GusApi\GusApi;
use GusApi\ReportTypes;
use GusApi\BulkReportTypes;

class Read extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context      $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data        $jsonHelper,
        \Psr\Log\LoggerInterface                   $logger,
        \Kowal\Gus\Helper\Config                   $config
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->config = $config;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
//            $gus = new GusApi('abcde12345abcde12345', 'dev');
            $klucz_uzytkownika = $this->config->getGeneralCfg('klucz_uzytkownika', 0);
            $gus = new GusApi($klucz_uzytkownika);

            try {
                $nipToCheck = $this->getRequest()->getParam('vat_id');
                $gus->login();

                $gusReports = $gus->getByNip($nipToCheck);

//                var_dump($gus->dataStatus());
//                var_dump($gus->getBulkReport(
//                    new DateTimeImmutable('2019-05-31'),
//                    BulkReportTypes::REPORT_DELETED_LOCAL_UNITS));

                foreach ($gusReports as $gusReport) {
                    //you can change report type to other one
                    $reportType = ReportTypes::REPORT_PERSON;
                    // echo $gusReport->getName();
                    //  echo 'Address: '. $gusReport->getStreet(). ' ' . $gusReport->getPropertyNumber() . '/' . $gusReport->getApartmentNumber();

                    $fullReport = $gus->getFullReport($gusReport, $reportType);
                    return $this->jsonResponse(['200', $gusReport]);
                }


            } catch (InvalidUserKeyException $e) {
                $result = 'Bad user key';
                return $this->jsonResponse(['400', $result]);

            } catch (NotFoundException $e) {

                $result = 'No data found for NIP: ' . $nipToCheck . '<br>';
                $result .= 'For more information read server message below: <br>';
                $result .= $gus->getResultSearchMessage();

                return $this->jsonResponse(['400', $result]);

            }

            return $this->jsonResponse('your response');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
